# 隐藏进程（X86 + X64）

## 开发工具安装...
- 安装VS2010编译支持x86环境 cn_visual_studio_2010_professional_x86_dvd_532145.iso
- 安装VS2015编译支持x64环境 cn_visual_studio_professional_2015_x86_x64_dvd_6846645.iso

## 用VS2015打开源码...
- x64Hider
- x64Payload
- x86Hider (Visual Studio 2010)
- x86Payload

## 编译代码生成...
- BuildOutput\x64Hider.exe
- BuildOutput\x86Hider.exe

## 运行说明...
- x64Hider.exe -i <pid> -n <name> -x <name>
- x86Hider.exe -i <pid> -n <name> -x <name>

## 验证
- 打开记事本 `notepad` 和任务管理器 `taskmgr`.
- 以管理员权限运行命令行 `cmd`.
- 执行 `x64Hider.exe -n notepad.exe`.
- 查看任务管理器，记事本在任务管理器中消失了

## 提示
- 项目>属性>常规>MFC的使用=在静态库中使用MFC
- 项目>属性>C/C++>代码生成>运行库=多线程
- 项目>属性>链接器>调试>生成调试信息=否
