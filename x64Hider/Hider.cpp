#include "stdafx.h"
#include "../Hide/Daemon.h"
#include "../Hide/Preparations.h"
#include "../Hide/ArgumentsPassing.h"

int _tmain(int argc, _TCHAR* argv[])
{
	if (PrepareContents(argc, argv)) {
		LaunchDaemon();
	}
}