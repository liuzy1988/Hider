function Get-HexString
{
	param([string] $fileName)
	$fileBytes=[System.IO.File]::ReadAllBytes($PSScriptRoot.ToString()+$fileName)
	return [System.BitConverter]::ToString($fileBytes)
}
function Get-ByteString
{
	param([string] $fileName)
	$fileBytes=[System.IO.File]::ReadAllBytes($PSScriptRoot.ToString()+$fileName)
	return ($fileBytes)
}
$x86PayloadHex=Get-ByteString '\BuildOutput\x86Payload.dll'
$x86PayloadLen=$x86PayloadHex.Length
$x86PayloadHex=$x86PayloadHex -join ","
$x64PayloadHex=Get-ByteString '\BuildOutput\x64Payload.dll'
$x64PayloadLen=$x64PayloadHex.Length
$x64PayloadHex=$x64PayloadHex -join ","
$output86CFile=$PSScriptRoot.ToString()+"\x86Hider\DLLs_hex.cpp"
$output64CFile=$PSScriptRoot.ToString()+"\x64Hider\DLLs_hex.cpp"

"---------  Started Building x86DLLs script ---------"

New-Item $output86CFile -ItemType file -Force | Out-Null
'#include "stdafx.h"'|Out-File -Encoding utf8 -FilePath $output86CFile -Append
'#include "..\Hide\DLLs_hex.h"'|Out-File -Encoding utf8 -FilePath $output86CFile -Append
'BYTE tmpx86PayloadByteArr[]={'+($x86PayloadHex.Split("-") -join "")+'};'|Out-File -Encoding utf8 -FilePath $output86CFile -Append
'BYTE *x86PayloadByteArr=tmpx86PayloadByteArr;'|Out-File -Encoding utf8 -FilePath $output86CFile -Append
'int x86PayloadSize='+$x86PayloadLen+';'|Out-File -Encoding utf8 -FilePath $output86CFile -Append
'BYTE *x64PayloadByteArr=tmpx86PayloadByteArr;'|Out-File -Encoding utf8 -FilePath $output86CFile -Append
'int x64PayloadSize=x86PayloadSize;'|Out-File -Encoding utf8 -FilePath $output86CFile -Append

"---------  Started Building x64DLLs script ---------"

New-Item $output64CFile -ItemType file -Force | Out-Null
'#include "stdafx.h"'|Out-File -Encoding utf8 -FilePath $output64CFile -Append
'#include "..\Hide\DLLs_hex.h"'|Out-File -Encoding utf8 -FilePath $output64CFile -Append
'BYTE tmpx86PayloadByteArr[]={'+($x86PayloadHex.Split("-") -join "")+'};'|Out-File -Encoding utf8 -FilePath $output64CFile -Append
'BYTE *x86PayloadByteArr=tmpx86PayloadByteArr;'|Out-File -Encoding utf8 -FilePath $output64CFile -Append
'int x86PayloadSize='+$x86PayloadLen+';'|Out-File -Encoding utf8 -FilePath $output64CFile -Append
'BYTE tmpx64PayloadByteArr[]={'+($x64PayloadHex.Split("-") -join "")+'};'|Out-File -Encoding utf8 -FilePath $output64CFile -Append
'BYTE *x64PayloadByteArr=tmpx64PayloadByteArr;'|Out-File -Encoding utf8 -FilePath $output64CFile -Append
'int x64PayloadSize='+$x64PayloadLen+';'|Out-File -Encoding utf8 -FilePath $output64CFile -Append

"---------  Finished Building-DLLs script ---------"