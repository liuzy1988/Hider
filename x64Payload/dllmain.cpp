// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "Payload.h"

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	pArgStruct args = (pArgStruct)lpReserved;

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH://当这个DLL被映射到了进程的地址空间时
		if (!isUp)
		{
#ifdef DEBUG_MODE
			PrintToFile("Injected!");
#endif // DEBUG
			isUp = true;
			InitializeDLL(args);
		}
		break;
	case DLL_THREAD_ATTACH://一个线程正在被创建
		break;
	case DLL_THREAD_DETACH://线程终结
		break;
	case DLL_PROCESS_DETACH://这个DLL从进程的地址空间中解除映射
		isUp = false;
		UnhookDLL();
		break;
	}
	return TRUE;
}